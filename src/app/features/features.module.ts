import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { SharedModule } from '../shared';
import { ContactComponent } from './contact/contact.component';
import { PricingComponent } from './pricing/pricing.component';
import { FeaturesRoutingModule } from './features-routing.module';
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';

@NgModule({
  declarations: [
    HomeComponent,
    ServicesComponent,
    ContactComponent,
    PricingComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FeaturesRoutingModule,
    HighlightModule

  ],
  providers: [
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        coreLibraryLoader: () => import('highlight.js/lib/core'),
        languages: {
          typescript: () => import('highlight.js/lib/languages/typescript'),
          css: () => import('highlight.js/lib/languages/css'),
        },
    }
  }
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  exports:[]
})
export class FeaturesModule { }
