import { Component, OnInit } from '@angular/core';
import { EventHandlerService } from 'src/app/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  constructor(private eh:EventHandlerService) { }

  ngOnInit(): void {
  }

  hideOrshow(){
    var plus:any=document.querySelectorAll('.fa-plus')
    var m:any=document.querySelectorAll('.fa-minus')

    for(let item of plus){
      this.eh.hideOrshow(item)
    }

    for(let item of m ){
      this.eh.hideOrshow(item)
    }

  this.eh.hideOrshow(event)

  var card:any=document.querySelectorAll('.card-service')
  for(let item of card ){
    this.eh.hideOrshow(item)

  }

  }

}
