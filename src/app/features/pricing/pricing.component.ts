import { Component, OnInit } from '@angular/core';
import { EventHandlerService } from 'src/app/core';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  constructor(private eh:EventHandlerService) { }

  ngOnInit(): void {
  }

  hideOrshow(event:any){
    console.log(event)
    var plus:any=document.querySelectorAll('.p')
    var m:any=document.querySelectorAll('.m')

    for(let item of plus){
      this.eh.hideOrshow(item)
    }

    for(let item of m ){
      this.eh.hideOrshow(item)
    }

  this.eh.hideOrshow(event)

  var card:any=document.querySelectorAll('.b')
  for(let item of card ){
    this.eh.hideOrshow(item)

  }

  }

}
