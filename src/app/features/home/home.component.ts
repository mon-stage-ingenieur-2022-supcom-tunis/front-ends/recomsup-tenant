
import { from } from 'rxjs';
import { Component, OnInit ,Input} from '@angular/core';
import { EventHandlerService } from 'src/app/core';
import {  HighlightJS } from 'ngx-highlightjs';
import hljs from 'highlight.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
@Input() signUp=false
python=false
nodejs=true
api=false
element:any;

code=`
 // installation
npm  install recomSup --save
// get 5 recommendations for user with id =145
const recomsup= require("recomSup")
const recommendations = async (user,number)=>{
  return await recomsup.itemBased.recommend(user,number)
}
console.log(recommendations(145,5))
`;

  constructor(private eh:EventHandlerService) {

  }

  ngOnInit(): void {
  this.eh.SignUp.subscribe(res=>{
   this.signUp=res


})


  }

  signup(){
      this.signUp=true;
  }

  setenv(){
    this.nodejs=true
    this.api=false
    this.python=false;
    this.code=`// installation
    npm  install recomSup --save
    // get 5 recommendations for user with id =145
    const recomsup= require("recomSup")
    let recommendations = await recomsup.itemBased.recommend(user=145,5)
    console.log(recommendations)


    `

  }
  setenv2(){
    this.nodejs=false
    this.api=true
    this.python=false;
    this.code=`// send request to our rest api to recommend 5 items to user 45
    curl 'https://recomsup.tn/api/v1/45/5'

    `
  }
  setenv3(){
    this.nodejs=false
    this.api=false
    this.python=true;
    this.code=`
    # installation
    pip install  recomSup
    # import itemBased module
    form recomSup.itemBased import *

    def recommend(user,number):
        return recomsup.recommend(user,number)

    if __name__=='__main__':
        recommendations= recommend(user=45,10)
        print(recommendations)

    `
  }


  cancel(val:boolean){
       this.signUp=false
  }

  top(){
    window.scroll(0, 0)
  }

  down(){
    window.scroll(0, document.body.scrollHeight)
  }

 
    View(){
      let view=document.getElementById('recom1')
      view?.scrollIntoView({behavior:'smooth'})
    }

   
    
  
}
