import { Injectable } from '@angular/core';
import { compress, decompress } from 'compress-json'

const USER='USER';
const TOKEN ='TOKEN'
@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  saveUser(data:any,callback:any){
    try{
      let compressed=compress(data)
      localStorage.setItem(USER,JSON.stringify(compressed))
    }catch(err){
      callback(err)
    }
  }

async getUser(){
 let compressed :any= localStorage.getItem(USER);
 compressed=JSON.parse(compressed)
 return await decompress(compressed);
}


saveToken(data:any,callback:any){
  try{
    let compressed=compress(data)
    localStorage.setItem(TOKEN,JSON.stringify(compressed))
  }catch(err){
    callback(err)
  }
}

 getToken(){
  let Token:any= localStorage.getItem(TOKEN);
  return Token
 }

SignOut(){
  localStorage.removeItem(TOKEN);
}

}
