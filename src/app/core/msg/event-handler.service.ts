import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EventHandlerService {
Login=new Subject<boolean>()
SignUp=new Subject<boolean>()
settings= new Subject<boolean>()
items= new Subject<boolean>()
users= new Subject<boolean>()
integrate= new Subject<boolean>()
analitics= new Subject<boolean>()
logOut = new Subject<boolean>()
dashboard= new Subject<boolean>()
activeModal = new Subject<any>()
  constructor() { }

  triggerMouse(event:any){
    var evt = new MouseEvent("click", {
      view: window,
      bubbles: true,
      cancelable: true,
  });
  event.dispatchEvent(evt);

  }

  hideOrshow(event:any){
   if(event.hidden==false){
     event.hidden=true
   }else{
    event.hidden=false

   }
  }
}
