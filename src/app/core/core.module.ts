import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventHandlerService } from './msg/event-handler.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers:[EventHandlerService]
})
export class CoreModule { }
