import { Injectable, Inject, Optional } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { TokenService } from '../TokenService';
@Injectable()
export class UniversalAppInterceptor implements HttpInterceptor {

  constructor( private authService: TokenService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const token = this.authService.getToken();
    req = req.clone({
      url:  req.url,
      setHeaders: {
        Authorization: `x-access-token ${token}`
      }
    });
    return next.handle(req);
  }
}
