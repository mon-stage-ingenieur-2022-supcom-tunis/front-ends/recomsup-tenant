import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from '../../TokenService';
declare var require:any;
const jwt_decode :any = require('jwt-decode')

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private tokenService:TokenService){}
  jwtToken:any
  decodedToken:any
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if(this.getToken()){
        return true

      }else{
        location.href='/'
        return false
      }
  }

  getToken(){
   return this.tokenService.getToken()

  }

  decodeToken() {
    this.getToken()
    this.decodedToken = jwt_decode.jwt_decode(this.jwtToken);
    console.log(this.decodeToken)
  }

}
