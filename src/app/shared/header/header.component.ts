import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { EventHandlerService } from 'src/app/core/msg';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private modalService: NgbModal,private eh:EventHandlerService) { }
  public isCollapsed :any= false;
  closeResult = '';
username:string='@dave'
password:string='password'
@Output() sign = new EventEmitter<boolean>()

  ngOnInit(): void {
  }

  login(){
    location.href='/dashboard'

  }

  open(content:any) {

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.login()
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


dismiss(){
  //this.modalService.dismiss('Cross click')
}

register(){
  this.eh.SignUp.next(true)
  document.getElementById('reg')?.scrollIntoView()

  var d:any= document.querySelector('#diss')
  this.eh.triggerMouse(d)
}

contact(){
 document.getElementById('contact')?.scrollIntoView()
}

product(){
  document.getElementById('Product')?.scrollIntoView()
 }

 search(){
  document.getElementById('search')?.scrollIntoView()
 }

 Content(){
  document.getElementById('content')?.scrollIntoView()
 }


 pricing(){
  document.getElementById('pricing')?.scrollIntoView({
    behavior:"smooth"
  })

 }

 language(event:any){
var l:any= document.querySelector('#languague');
l.innerText= event?.target.id
 }

 recom1(){
   document.getElementById('recom1')?.scrollIntoView({
     behavior:"smooth"
   })
 }
 recom2(){
  document.getElementById('recom2')?.scrollIntoView({
    behavior:"smooth"
  })
}



}
