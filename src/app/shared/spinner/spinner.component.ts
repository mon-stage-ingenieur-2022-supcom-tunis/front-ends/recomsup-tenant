import { Component, OnInit, Output ,Input} from '@angular/core';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
gif='https://cdn.dribbble.com/users/421466/screenshots/2379575/replace-2r-400px.gif';
timer :any
@Input() stop=false
  constructor() { }

  ngOnInit(): void {

    Swal.fire({
      html:`<img src='${this.gif}'> <br> loading...`,
      showConfirmButton:false
    })

   }


   toggle(){
     clearInterval(this.timer)
   }
}
