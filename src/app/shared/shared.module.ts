import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SpinnerComponent } from './spinner/spinner.component';

//import {HttpM}

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    LoginFormComponent,
    SignUpFormComponent,
    DashboardComponent,
    SpinnerComponent,
    


  ],

  exports:[FooterComponent,HeaderComponent,SignUpFormComponent,LoginFormComponent,FormsModule,DashboardComponent,SpinnerComponent
  ],

  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    
    
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
