import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ApiService } from 'src/app/core';
import swal from 'sweetalert2'
@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss']
})
export class SignUpFormComponent implements OnInit {
@Output() cl =new EventEmitter<boolean>()
@Output() registerData= new EventEmitter<any>();
@Output() Login=new EventEmitter<boolean>();
name:string='first name';
last_name:string='last name';
passWord:string='password';
password:string="password";
email:string='dave.ndri@supcom.tn';
username:string='@dave';
company:string='aladin';
website:string='https://recomsup.tn';
stop=false;
login=false;

  constructor(private api:ApiService) { }

  ngOnInit(): void {


  }
async register(){

if(this.name && this.last_name &&this.email&&this.company &&this.passWord){
  if(!this.stop){
    this.toggle()

  }

  await  this.api.SignUp({email:this.email,password:this.password,web:this.website,firstname:this.name,lastname:this.last_name,organisation:this.company,name:this.generateString()[1],username:this.username,dev:this.generateString()[0]}).subscribe(
      res=>{
        this.toggle()
        this.registerData.next(res.response)
swal.fire({
     icon:"success",
     text:'You\'re successfuly registered go ahead and sign in to your RecomSup dashboard'

}).then((rs)=>{
this.Login.next(true)
location.reload()
})


      },
      err=>{
        swal.fire({
          icon:"error",
          text:'Sorry you did not provide valid credential check the provided credentials'
     });
     this.toggle()
     console.log(err);
      }
    )
}
}

toggle(){
  this.stop=!this.stop
}
cancel(val:any){
  this.cl.emit(true)
}

generateString(){
  var characters = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
  var lenString=2
  let randomstringdev:any;
  let randomstring:any;

  for (var i=0; i<lenString; i++) {
    var rnum = Math.floor(Math.random() * characters.length);
    randomstring += characters.substring(rnum, rnum+1);
    randomstringdev += characters.substring(rnum, rnum);

}
return ['recomSup_'+randomstringdev +this.company.trim()+'_dev','recomSup_'+randomstring +this.company.trim()+'_pro']
}
}
