import { Component, OnInit } from '@angular/core';
import { EventHandlerService, TokenService } from 'src/app/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private eh:EventHandlerService,private tokenService:TokenService) { }

  ngOnInit(): void {

  }


  setting(){
    this.eh.settings.next(true)
  }
  analytic(){
    this.eh.analitics.next(true)
  }

item(){
  this.eh.items.next(true)
}

logOut(){
  try{
    this.tokenService.SignOut()
    location.href='/'
  }catch(err){
    console.log(err)
  }
  //this.eh.logOut.next(true)
}

integrate(){
  this.eh.integrate.next(true)
}

default(){
  this.eh.dashboard.next(true)
}

users(){
  this.eh.users.next(true)
}

  setActive(){
     var sidebar :any= document.querySelector('.sidebar')
     sidebar.classList.toggle('active')
  }
}
