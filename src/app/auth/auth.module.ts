import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGateComponent } from './auth-gate/auth-gate.component';
import { SharedModule } from '../shared';
import { AuthComponent } from './auth/auth.component';
import { AuthGateRoutingModule } from './auth-gate/auth-gate-routing.module';



@NgModule({
  declarations: [
    AuthGateComponent,
    AuthComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AuthGateRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthModule { }
